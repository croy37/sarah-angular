/**
 * Created by croy on 2016-02-28.
 */
'use strict';

var app = angular.module('Portfolio', [
    'ngRoute',
    'ngResource'
]);