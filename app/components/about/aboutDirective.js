/**
 * Created by croy on 2016-02-28.
 */

app.directive('portfolioAbout', function(){
    return{
        restrict: 'E',
        templateUrl: 'app/components/about/aboutTemplate.html'
    };
});