/**
 * Created by croy on 2016-02-28.
 */

app.directive('portfolioSplashImage', function(){
   return {
       restrict: 'E',
       templateUrl: 'app/components/splashImage/splashImageTemplate.html'
   };
});