/**
 * Created by croy on 2016-02-28.
 */

app.directive('portfolioGallery', function(){
    return {
        restrict: 'E',
        templateUrl: 'app/components/gallery/galleryTemplate.html',
        controller: 'galleryController',
    };
})
.directive('portfolioItem', function(){
    return function(scope, element) {
        if (scope.$last){
            $(".work-btn").on("click", function(eventObject){
                eventObject.preventDefault();

                $('.work-details[data-state="open"]').attr("data-state", "closed");

                var id= $(this).attr("href");
                $(id).attr("data-state", "open");

                $('html, body').stop().animate({
                    scrollTop: $("#work-position").offset().top
                }, 1500,'easeInOutExpo');
            });
        }
    };
})
.directive('portfolioClose', ['$timeout', function($timeout){
    return function(scope, element){
        if(scope.$last)
        {
            $(".work-close").on("click", function(eventObject){
                $(this).parents(".work-details").attr("data-state", "closed");
            });
        }

        $timeout(function(){
            $('.flexslider').flexslider({
                animation: "slide"
            });
        });
    }
}]);