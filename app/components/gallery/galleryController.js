/**
 * Created by croy on 2016-02-28.
 */

app.controller('galleryController', ['$scope', 'Gallery', function($scope, Gallery){
    $scope.firstName = "Test First Name";
    $scope.lastName = "Test Last Name";

    Gallery.all().success(function(data){
        $scope.galleryItems = data;
    });
}]);