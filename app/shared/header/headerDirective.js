/**
 * Created by croy on 2016-02-28.
 */
(function(){
    app.directive('portfolioHeader', function(){
        return{
            restrict: 'E',
            templateUrl: './app/shared/header/headerTemplate.html',
            link: function(scope, element){
                $('nav a').on('click',function(event){
                    var $anchor = $(this);

                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top
                    }, 1500,'easeInOutExpo');
                    event.preventDefault();
                });
            }
        };
    });
})();