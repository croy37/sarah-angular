/**
 * Created by croy on 2016-02-28.
 */

app.directive('portfolioContact', function(){
    return {
        restrict: 'E',
        templateUrl: 'app/shared/contact/contactTemplate.html',
        link: function(scope, element){
            $('.go-top').on('click',function(event){
                var $anchor = $(this);

                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top
                }, 1500,'easeInOutExpo');
                event.preventDefault();
            });
        }
    };
});